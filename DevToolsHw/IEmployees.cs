﻿using System;
using System.Collections.Generic;

namespace DevToolsHw
{
	interface IEmployees
	{
		int Add(String name, int salary); // returns ID
		ICollection<int> GetAll(); // returns a set of IDs
		String GetName(int id);
		int GetSalary(int id);
		void ChangeSalary(int id, int newSalary);
	}
}
