﻿using System;
using System.Linq;

namespace DevToolsHw
{
	class Statistics
	{
		private IEmployees dataSource;

		public Statistics(IEmployees dataSource)
		{
			this.dataSource = dataSource;
		}

		public int ComputeAverageSalary()
		{
			var ids = dataSource.GetAll();

			return (int)ids.Average(x => dataSource.GetSalary(x));
		}

		public int GetMinSalary()
		{
			return dataSource.GetAll().Min(x => dataSource.GetSalary(x));
		}

		public void PrintSalariesByName() // prints the list of pairs <name, salary> that is sorted by employee names
		{
			var ids = dataSource.GetAll();

			foreach (var id in ids.OrderBy(x => dataSource.GetName(x)))
			{
				Console.WriteLine(dataSource.GetName(id) + " " + dataSource.GetSalary(id));
			}
		}
	}
}
