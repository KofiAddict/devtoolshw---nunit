﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DevToolsHw
{
	[TestFixture(Author = "Petr Jarochy", Category = "SortedDictionary",
		Description = "Testing basic functionality of SortedDictionary")]
	public class SortedDictionaryTesting
	{
		private SortedDictionary<string, string> testSortedDictionary;
		private List<string> keys;
		private int originalCount;

		[SetUp]
		public void Setup()
		{
			testSortedDictionary = new SortedDictionary<string, string>();
			keys = new List<string>();

			for (int i = 0; i < 50; i++)
			{
				testSortedDictionary.Add("key" + i, "value" + i);
				keys.Add("key" + i);
			}

			originalCount = testSortedDictionary.Count;
		}

		[Test]
		public void TestAdd()
		{
			const string key = "AddedKey";
			const string value = "AddedValue";
			testSortedDictionary.Add(key, value);

			Assert.That(value, Is.EqualTo(testSortedDictionary[key]));
			Assert.That(originalCount + 1, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestAddExisting()
		{
			Assert.That(() => testSortedDictionary.Add("key1", "somevalue"), Throws.ArgumentException,
				"Adding existing key was successful or threw a different exception");

			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestRemove()
		{
			testSortedDictionary.Remove("key3");

			Assert.That(testSortedDictionary.ContainsKey("key3"), Is.False);
			Assert.That(originalCount - 1, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestRemoveNonexistant()
		{
			Assert.That(() => testSortedDictionary.Remove("somerandomkey"), Is.False);
			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestRemoveAll()
		{
			foreach (var key in testSortedDictionary.Keys.ToList())
			{
				testSortedDictionary.Remove(key);
			}

			Assert.That(testSortedDictionary, Is.Empty);
		}

		[Test]
		[TestCase("key1")]
		[TestCase("key5")]
		public void TestGetValue(string byKey)
		{
			Assert.That(() => testSortedDictionary[byKey], Throws.Nothing);

			Assert.That(testSortedDictionary[byKey], Is.EqualTo("value" + byKey.Remove(0, 3)));

			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestGetNonexistingValue()
		{
			Assert.That(() => testSortedDictionary["somerandomerkey"], Throws.TypeOf(typeof(KeyNotFoundException)));
			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		[TestCase("key1")]
		[TestCase("key8")]
		[TestCase("nonexistingkey")]
		public void TestContainingKey(string key)
		{
			Assert.That(testSortedDictionary.ContainsKey(key), Is.EqualTo(keys.Contains(key)));
			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestKeysSorted()
		{
			var lastkey = string.Empty;

			foreach (var key in testSortedDictionary.Keys)
			{
				Assert.That(key, Is.GreaterThanOrEqualTo(lastkey));
				lastkey = key;
			}

			Assert.That(originalCount, Is.EqualTo(testSortedDictionary.Count));
		}

		[Test]
		public void TestToFail()
		{
			Assert.That(testSortedDictionary.Remove("somerandomestkey"), "Key that was to be removed was not found in the Sorted Dictionary");
		}
	}
}
