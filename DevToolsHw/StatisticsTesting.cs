﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace DevToolsHw
{
	// this deals with test cases
	[TestFixture(0)]
	[TestFixture(20)]
	[TestFixture(100)]
	class StatisticsTesting
	{
		private Statistics testStatistics;
		private IEmployees employees;
		private readonly int employeeCount;

		public StatisticsTesting(int employeeCount)
		{
			this.employeeCount = employeeCount;
		}

		[SetUp]
		public void Setup()
		{
			employees = new Employees(); // later exchange for different implementation

			for (int i = 0; i < employeeCount; i++)
			{
				employees.Add("Employee num" + i, i * 100);
			}

			testStatistics = new Statistics(employees);
		}

		[Test]
		public void TestNullDataSource()
		{
			// id expect this to happen
			Assert.That(() => new Statistics(null), Throws.ArgumentNullException, "Statistics can be constructed with null.");
		}

		[Test]
		public void TestAverageSalary()
		{
			var average = 0;
			var computedAverage = testStatistics.ComputeAverageSalary();

			if (employees.GetAll().Any())
			{
				average = (int)employees.GetAll().Average(x => employees.GetSalary(x));
				Assert.That(computedAverage, Is.EqualTo(average), "Function returned different average than expected. Expected: " + average + ", Got: " + computedAverage);
			}
			else
			{
				// id hope doc would state, what happens in this case, id expect zero to be returned, or -1
				// other option is to assert for throwing exceptions
				Assert.That(computedAverage, Is.EqualTo(average), "Function returned average different than 0 for 0 employees in data source. Function returned: " + computedAverage);
			}
		}

		[Test]
		public void TestMinimalSalary()
		{
			var minimum = 0;
			var computedMin = testStatistics.GetMinSalary();
			// If there is any employees
			if (employees.GetAll().Any())
			{
				minimum = employees.GetAll().Min(x => employees.GetSalary(x));
				Assert.That(computedMin, Is.EqualTo(minimum), "Statistics returned unexpected value for AverageSalary, expected: " + minimum + ", got: " + computedMin);
			}
			// if there is no employees
			else
			{
				// id hope doc would state, what happens in this case, id expect zero to be returned, or -1
				// other option is to assert for throwing exceptions
				Assert.That(computedMin, Is.EqualTo(minimum), "For zero employees, statistics returned different value than zero. Method returned: " + computedMin);
			}
		}

		[Test]
		public void TestPrintingOfEmployees()
		{
			using (var fileStream = new FileStream("./temp", FileMode.Create))
			{
				using (TextWriter tw = new StreamWriter(fileStream))
				{
					// set console out to file
					Console.SetOut(tw);

					testStatistics.PrintSalariesByName();

					tw.Close();
					fileStream.Close();

					using (TextReader tr = new StreamReader(new FileStream("./temp", FileMode.Open)))
					{
						string computedOut = tr.ReadToEnd();
						string expectedOut = employees.GetAll().OrderBy(x => employees.GetName(x))
							.Select(x => employees.GetName(x) + " " + employees.GetSalary(x))
							.Aggregate((x, y) => x + Environment.NewLine + y);
						expectedOut += Environment.NewLine;

						tr.Close();

						Assert.That(computedOut, Is.EqualTo(expectedOut)); 
					}
				} 
			}
		}

		[TearDown]
		public void TearDown()
		{
			if (File.Exists("./temp"))
			{
				File.Delete("./temp");
			}
		}

		private class Employees : IEmployees
		{
			private readonly Dictionary<int, Employee> EmployeesDictionary = new Dictionary<int, Employee>();
			private int highestId = 1;

			public int Add(string name, int salary)
			{
				++highestId;
				EmployeesDictionary.Add(highestId, new Employee() {Name = name, Salary = salary});
				return highestId;
			}

			public ICollection<int> GetAll()
			{
				return EmployeesDictionary.Keys.ToList();
			}

			public string GetName(int id)
			{
				return EmployeesDictionary[id].Name;
			}

			public int GetSalary(int id)
			{
				return EmployeesDictionary[id].Salary;
			}

			public void ChangeSalary(int id, int newSalary)
			{
				EmployeesDictionary[id].Salary = newSalary;
			}

			private class Employee
			{
				public string Name;
				public int Salary;
			}
		}
	}
}
